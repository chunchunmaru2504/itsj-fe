export class Authentication {

    static getEmail(): string {
        return localStorage.getItem("email");
    }

    static getFullName(): string {
        let firstName = localStorage.getItem("firstName");
        let lastName = localStorage.getItem("lastName");
        return firstName + " " + lastName;
    }
}