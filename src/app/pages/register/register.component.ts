import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  formRegister: FormGroup;
  confirmPassword: string = '';

  constructor(
    private userService: UserService,
    private router: Router) { 
    this.formRegister = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', Validators.required),
      confirmPassword: new FormControl('', Validators.required),
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      dateOfBirth: new FormControl('', Validators.required),
    });
  }
  
  get f() {
    return this.formRegister.controls;
  }

  ngOnInit() {
  }

  submit() {
    this.userService.register(this.formRegister.value).subscribe(
      res => {
        alert("Register successfully");
        this.router.navigate(['/login']);
      },
      error => {
        if (error.error.message == "email-is-existed") {
          alert("Email is existed!!!");
        }
      }
    )
  }

}
