import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-infomation',
  templateUrl: './infomation.component.html',
  styleUrls: ['./infomation.component.css']
})
export class InfomationComponent implements OnInit {

  user: User;

  formInformation: FormGroup;

  constructor(private userService: UserService) {
    this.formInformation = new FormGroup({
      id: new FormControl(''),
      email: new FormControl(''),
      password: new FormControl('', Validators.required),
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      dateOfBirth: new FormControl('', Validators.required),
      role: new FormControl(''),
    });
   }

  ngOnInit() {
    this.getInformation();
  }

  get f () {
    return this.formInformation.controls;
  }

  getInformation() {
    this.userService.getInformation().subscribe(
      res => {
        this.formInformation.patchValue(res);
        this.formInformation.controls['password'].setValue('password');
      }
    )
  }

  submit() {
    this.userService.editInformation(this.formInformation.value).subscribe(
      res => {
        alert("Update Information successfully")
        this.getInformation();
      }
    )
  }

}
