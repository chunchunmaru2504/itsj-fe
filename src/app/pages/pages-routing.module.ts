import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { InfomationComponent } from './infomation/infomation.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'information', component: InfomationComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
