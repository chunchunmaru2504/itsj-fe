import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  formLogin: FormGroup;
  isWrong: boolean;

  constructor(
    private userService: UserService,
    private router: Router) {
    this.formLogin = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, Validators.required)
    });
  }

  ngOnInit() {
    sessionStorage.clear();
    localStorage.clear();
  }

  get f() {
    return this.formLogin.controls;
  }

  login() {
    this.userService.login(this.formLogin.value)
    .subscribe(
      res => {
        localStorage.setItem ('Token', res.token);
        this.getInformation();
        this.router.navigate(['/']);
      },
      error => {
        this.isWrong = true;
      }
    );
  }

  getInformation() {
    this.userService.getInformation().subscribe(
      res => {
        localStorage.setItem("user", JSON.stringify(res));
      }
    ) 
  }

}
