import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Product } from 'src/app/core/models/product';
import { CartRepository } from 'src/app/core/repository/cart.repositori';
import { ProductService } from 'src/app/core/services/product.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  products: Array<Product>;

  sortNames: any;
  sortTypes: any;
  sortField: string = 'name';
  sortType: string = 'desc';
  keyWord: string = '';
  size: number = 5;
  page: number = 0;

  constructor(
    private productService: ProductService,
    private route: ActivatedRoute) {
    this.sortNames = [
      { name: 'Name', value: 'name' },
      { name: 'Price', value: 'price' },
    ];

    this.sortTypes = [
      { name: 'Cao đến thấp', value: 'desc' },
      { name: 'Thấp đến cao', value: 'asc' },
    ];
  }

  ngOnInit() {
    this.keyWord = this.route.snapshot.queryParamMap.get('key-word');
    this.getProducts();
  }

  searchRequest(): any {
    return {
      size : this.size,
      page : this.page,
      sortField : this.sortField,
      sortType : this.sortType
    }
  }

  getProducts() {
    this.page = 0;
    let keyWord = this.keyWord;
    let request = this.searchRequest();

    this.productService.search(keyWord, request).subscribe(
      res => {
        this.products = res.content;
      }
    )
  }

  more() {
    this.page += 1;
    let keyWord = this.keyWord;
    let request = this.searchRequest();

    this.productService.search(keyWord, request).subscribe(
        res => {
          for (let i = 0; i < res.content.length; i++) {
            this.products.push(res.content[i]);
          }
        },
        error => {
          console.log(error);
        }
      )
  }

  addItem(product: Product) {
    CartRepository.addCart(product);
  }

  buyProduct(product: Product) {
    product.totals = 1;
    let request = {
      products : [product]
    }

    this.productService.buy(request).subscribe(
      res => {
        alert("Thanh toán thành công. Vui lòng kiểm tra E-mail");
      }
    )
  }
}
