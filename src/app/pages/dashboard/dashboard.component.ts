import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Product } from 'src/app/core/models/product';
import { ProductService } from 'src/app/core/services/product.service';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  formProduct: FormGroup;
  products: Array<Product>;

  searchProduct: any;
 
  keyWord: string = '';
  size: number = 5;
  page: number = 0;
  sortField: string = '';
  sortType: string = 'desc';
  totalPages: number[] = [];

  imageFile: File;
  srcImageFile: any;

  isLoading: boolean = false;

  constructor(
    private productService: ProductService,
    private userService: UserService,
    private router: Router) { }

  ngOnInit() {
    this.getInformation();
    this.initFormProduct();
    this.getProducts();
  }

  initFormProduct() {
    this.formProduct = new FormGroup({
      id: new FormControl(0),
      name: new FormControl('', Validators.required),
      price: new FormControl(0, Validators.min(1)),
      totals: new FormControl(0, Validators.min(1)),
      image: new FormControl(''),
    });
  }

  get f () {
    return this.formProduct.controls;
  }

  getInformation() {
    this.userService.getInformation().subscribe(
      res => {
        if (res) {
          if (res.role != "ADMIN") {
            this.router.navigate(['/login']);
          }
        }
      },
      error => {
        if (error.status == 403) {
          this.router.navigate(['/login']);
        }
      }
    );
  }

  sort(name: string) {
    this.sortType = this.sortType == "desc" ? "asc" : "desc";
    this.sortField = name;
    this.getProducts();
  }

  paging(page: number) {
    this.page = page;
    this.getProducts();
  }

  setSearchRequest() {
    this.searchProduct = {
      "size": this.size,
      "page": this.page,
      "sortField": this.sortField,
      "sortType": this.sortType
    }
  }

  getProducts() {
    this.setSearchRequest();
    this.productService.search(this.keyWord, this.searchProduct)
      .subscribe(
        res => {
          this.products = res.content;

          this.totalPages = [];
          for (let i = 0; i < res.totalPages; i++) {
            this.totalPages[i] = i;
          }
        },
        error => {
          console.log(error);
        }
      )
  }

  editProduct(product: Product) {
    this.formProduct.setValue(product);
  }

  chooseFile(file: File[]) {
    this.handleUpload(file[0]);
    this.imageFile = file[0];
  }

  handleUpload(file: File) {
    this.srcImageFile = null;
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
        this.srcImageFile = reader.result;
    };
  }

  createProduct() {
    this.isLoading = true;

    var formData = new FormData();
    if (this.imageFile) {
      formData.append('file', this.imageFile);
    }
    formData.append('product', JSON.stringify(this.formProduct.getRawValue()));

    this.productService.create(formData)
      .subscribe(
        res => {
          alert("Create success!");
          this.isLoading = false;
          this.getProducts();
          this.clearForm();
        },
        error => {
          this.isLoading = false;
        }
      )
  }

  updateProduct() {
    this.isLoading = true;

    var formData = new FormData();
    if (this.imageFile) {
      formData.append('file', this.imageFile);
    }
    formData.append('product', JSON.stringify(this.formProduct.getRawValue()));

    this.productService.update(this.f['id'].value, formData)
      .subscribe(
        res => {
          alert("Update success!");
          this.isLoading = false;
          this.getProducts();
          this.clearForm();
        },
        error => {
          this.isLoading = false;
        }
      )
  }

  deleteProduct() {
    this.isLoading = true;

    this.productService.delete(this.f['id'].value)
    .subscribe(
      res => {
          alert("Delete success!");
          this.isLoading = false;
        this.getProducts();
        this.clearForm();
      },
      error => {
        this.isLoading = false;
      }
    )
  }

  clearForm() {
    this.initFormProduct();
    this.imageFile = null;
    this.srcImageFile = null;
  }

}
