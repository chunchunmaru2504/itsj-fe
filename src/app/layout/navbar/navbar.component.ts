import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from 'src/app/core/models/product';
import { CartRepository } from 'src/app/core/repository/cart.repositori';
import { ProductService } from 'src/app/core/services/product.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  user: User;
  carts: Array<Product>;

  keyWord: string = '';
  formSearch: FormGroup;
  searchResult: any = [];

  constructor(
    private productService: ProductService,
    private router: Router,
    private route: ActivatedRoute) {

  }

  ngOnInit() {
    this.keyWord = this.route.snapshot.queryParamMap.get('key-word');
    this.user = JSON.parse(localStorage.getItem("user"));
    this.carts = CartRepository.getCart();
  }

  search() {
    var searchRequest = {
      "size": 10,
      "page": 0,
      "sortField": "name",
      "sortType": "desc"
    }

    this.productService.search(this.keyWord, searchRequest).subscribe(
      res => {
        this.searchResult = res.content;
      }
    )
  }

  async submit() {
    if (!this.keyWord) {
      this.keyWord = '';
    }

    this.router.navigate(['/home'], {
      queryParams: {
        'key-word': this.keyWord
      }
    });

    await this.delay(24);
    window.location.reload();
  }

  plusTotal(id: number) {
    CartRepository.plusTotal(id);
  }

  exceptTotal(id: number) {
    CartRepository.exceptTotal(id);
  }

  deleteItem(product: Product) {
    CartRepository.removeCart(product);
  }

  buyProducts() {
    let request = {
      products: CartRepository.getCart()
    }

    this.productService.buy(request).subscribe(
      res => {
        alert('Thanh toán thành công. Vui lòng xem hoá đơn trên E-mail');
        sessionStorage.removeItem("crarts");
        this.carts = [];
      }
    )
  }

  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
}
