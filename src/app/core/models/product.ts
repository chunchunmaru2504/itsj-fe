export class Product {
    private _id: number;
    private _name: string;
    private _price: number;
    private _totals: number;
    private _image: string;

    get id(): number { return this._id; }

    get name(): string { return this._name; }
    
    get price(): number { return this._price; }

    set price(price: number) {
        this._price = price;
    }

    get totals(): number { return this._totals; }

    set totals(totals: number) {
        this._totals = totals;
    }

    get image(): string { return this._image; }

    constructor(id: number, name: string, price: number, totals: number, image: string) {
        this._id = id;
        this._name = name;
        this._price = price;
        this._totals = totals;
        this._image = image
    }
}