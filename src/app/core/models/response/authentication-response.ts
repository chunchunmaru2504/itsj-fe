export class AuthenticationResponse {
    private _token: string;

    constructor(token: string) {
        this._token = token;
    }
    
    get token(): string {
        return this.token;
    }
}