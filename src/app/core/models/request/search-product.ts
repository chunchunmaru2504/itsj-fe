export class SearchProduct {
    private size: number;
    private page: number;
    private sortField: string;
    private sortType: string;

    constructor(size: number, page: number, sortField: string, sortType: string) {
        this.size = size;
        this.page = page;
        this.sortField = sortField;
        this.sortType = sortType;
    };
}