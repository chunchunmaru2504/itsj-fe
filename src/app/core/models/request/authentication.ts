export class Authentication {
  private email: string;
  private password: string;

  public get getEmail() : string {
    return this.email;
  }

  public get getPassword() : string {
    return this.password;
  }

  constructor(email: string, password: string) {
    this.email = email;
    this.password = password;
  }
}
