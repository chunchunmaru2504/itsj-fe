class User {
  private _email: string;
  private _password: string;
  private _firstName: string;
  private _lastName: string;
  private _dateOfBirth: Date;

  get email(): string {
    return this._email;
  }

  get password(): string {
    return this._password;
  }

  get firstName(): string {
    return this._firstName;
  }

  get lastName(): string {
    return this._lastName;
  }

  get dateOfBirth(): Date {
    return this._dateOfBirth;
  }

  set email(value: string) {
    this._email = value;
  }

  set password(value: string) {
    this._password = value;
  }

  set firstName(value: string) {
    this._firstName = value;
  }

  set lastName(value: string) {
    this._lastName = value;
  }

  set dateOfBirth(value: Date) {
    this._dateOfBirth = value;
  }
  
  constructor(email: string, password: string, firstName: string, lastName: string, dateOfBirth: Date) {
    this._email = email;
    this._password = password;
    this._firstName = firstName;
    this._lastName = lastName;
    this._dateOfBirth = dateOfBirth;
  }
}
