import { Product } from "../models/product";

export class CartRepository {
  private static carts: Array<Product> = [];

  static addCart(product: Product) {
    let result = this.carts.filter(cart => cart == product).length;
    if (!result) {
      product.totals = 1;
      this.carts.push(product);
      sessionStorage.setItem("carts", JSON.stringify(this.carts));
    }
  }

  static getCart(): Array<Product> {
    this.carts = JSON.parse(sessionStorage.getItem("carts"));
    return this.carts;
  }

  static plusTotal(id: number) {
    this.carts.map(cart => cart.id == id 
      ? cart.totals += 1 
      : cart.totals);
    sessionStorage.setItem("carts", JSON.stringify(this.carts));
  }

  static exceptTotal(id: number) {
    this.carts.map(cart => cart.id == id 
      ? cart.totals > 1 
        ?  cart.totals -= 1 
        : cart.totals
      : cart.totals);
    sessionStorage.setItem("carts", JSON.stringify(this.carts));
  }

  static removeCart(product: Product) {
    this.carts.splice(this.carts.indexOf(product), 1);
    sessionStorage.setItem("carts", JSON.stringify(this.carts));
  }

  static handleTotalPriceCart() {

  }
}