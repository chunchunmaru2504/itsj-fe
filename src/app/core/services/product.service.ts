import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Product } from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }

  search(keyWord: string, searchProduct: any) {
    return this.http.post<any>('/api/product/search', searchProduct, {
        params: new HttpParams().set("key_work", keyWord)
      }
    );
  }

  create(formData: any) {
    return this.http.post<any>('/api/product/create', formData);
  }

  update(id: number, formData: any) {
    return this.http.put<any>('/api/product/update', formData, {
        params: new HttpParams().set("id", id.toString())
      });
  }

  delete(id: string) {
    return this.http.delete<any>('/api/product/delete',{
      params: new HttpParams().set("id", id)
    });
  }

  buy(products: any) {
    return this.http.post<any>('/api/product/buy', products);
  }

}
