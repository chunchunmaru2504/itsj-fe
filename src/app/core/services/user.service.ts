import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Authentication } from '../models/request/authentication';
import { AuthenticationResponse } from '../models/response/authentication-response';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  login(authentication: Authentication) {
    return this.http.post<AuthenticationResponse>('/api/user/login', authentication);
  }

  register(json: any) {
    return this.http.post<any>('/api/user/register', json);
  }

  getInformation() {
    return this.http.get<any>('/api/user/get-information');
  }

  editInformation(json: any) {
    return this.http.put<any>('/api/user/edit-information', json);
  }

}
