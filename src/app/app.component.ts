import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from './core/services/user.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'itsj-fe';

  // constructor(
  //   private userService: UserService,
  //   private router: Router) {}

  // ngOnInit(): void {
  //   this.userService.getInformation().subscribe(
  //     res => {
  //       sessionStorage.setItem("user", JSON.parse(res));
  //     },
  //     error => {
  //       if (error.status == 403) {
  //         localStorage.clear();
  //         this.router.navigate(['/login']);
  //       }
  //     }
  //   )
  // }
}
